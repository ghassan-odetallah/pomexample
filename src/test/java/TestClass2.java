public class TestClass2 {

    private int firstNumber;
    private int secondNumber;

    public TestClass2(int firstNumber, int secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }
//    public int publicMember = 20;
//    protected int protectrdMember = 30;
//    int packagePrivate = 40;

}


