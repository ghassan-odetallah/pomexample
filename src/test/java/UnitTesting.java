import org.testng.Assert;
import org.testng.annotations.Test;

public class UnitTesting {

    public int summation(int a, int b) {

        return a-b;
    }

    @Test
    public void checkSummationMethod() {
        //System.out.println(summation(9,10));
        Assert.assertEquals(summation(10,9),1);
    }
}
