package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class LayalinaSearchPage extends BaseTest {

    public WebElement getSearchButton() {
        return searchButton;
    }

    @FindBy(xpath = "//header/div[@id='slide-nav']/div[2]/div[1]/ul[1]/li[11]/div[1]/a[1]/i[1]")
    private WebElement searchButton;
    @FindBy(xpath = "//input[@id='inpt_srch']")
    private WebElement searchText;
    @FindBy(xpath = "//button[@id='hdr_srch_btn']")
    private WebElement searchConfirm;




    public WebElement getCity() {
        return city;
    }

    @FindBy(xpath = "//*[@id=\"imsakiehCollapse\"]/ul/li[3]/div[2]/div/button/div/div/div")
    private WebElement city;

    public WebElement getSearchResult() {
        return searchResult;
    }

    @FindBy(xpath = "//body/div[3]/div[2]/section[1]/div[1]/article[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[1]/div[1]")
    private WebElement searchResult;


    public LayalinaSearchPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }


    public void layalinaSearch() {

        waitForElement(this.searchButton,10);

        this.searchButton.click();
        this.searchText.sendKeys("ghassan");
        //this.searchText.clear();
        this.searchConfirm.submit();

    }
}
