package pages;

import org.testng.Assert;
import org.testng.annotations.Test;

import static pages.GoogleSearchPage.getVerificationLinkText;

public class GoogleSearchTest extends BaseTest {

    @Test(description = " this is to verify that we can search by key on google " , priority = 2,dependsOnMethods = {"searchgoogleName"})
    public void searchgoogle() {

        GoogleSearchPage googleSearchPage = new GoogleSearchPage(driver);
        googleSearchPage.search("ghassan");
        Assert.assertTrue(getVerificationLinkText().contains("Ghassan"));

    }

    @Test(enabled = true)
    public void searchgoogleName() {

        GoogleSearchPage googleSearchPage = new GoogleSearchPage(driver);
        googleSearchPage.newSearch();

    }
}