package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import sun.lwawt.macosx.CSystemTray;

import java.util.ArrayList;

public class DriverFunctions extends BaseTest {


    @Test
    public void navigation() throws InterruptedException {

        //driver.get("https://www.layalina.com/");
        driver.navigate().to("https://www.layalina.com/");
        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.layalina.com/");
        System.out.println(driver.getCurrentUrl());
        driver.navigate().back();
        System.out.println(driver.getCurrentUrl());
        Thread.sleep(1000);
        driver.navigate().forward();
        System.out.println(driver.getCurrentUrl());
        Thread.sleep(1000);
        System.out.println(driver.getTitle() + "this is the titlte ");

        System.out.println(driver.getPageSource());
        //Assert.assertTrue(driver.getPageSource().contains("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">\n"));

    }

    @Test
    public void windowHandle() throws InterruptedException {

       // String originalWindow = driver.getWindowHandle();

        //assert driver.getWindowHandles().size() == 1;

        System.out.println(driver.getWindowHandles().size() + " this is the tab number");
        Thread.sleep(5000);

        // this is to open new tab
        ((JavascriptExecutor) driver).executeScript("window.open()");

        Thread.sleep(5000);
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());

        driver.switchTo().window(tabs.get(1));

        driver.get("https://www.layalina.com/");
        Thread.sleep(5000);

        System.out.println(driver.getWindowHandles().size() + " this is the tab number");

        System.out.println(driver.getTitle());

        driver.switchTo().window(tabs.get(0));

        Thread.sleep(10000);

    }

    @Test
    public void iframeHandle() throws InterruptedException {

        driver.get("https://jqueryui.com/selectable/");

        WebElement element = driver.findElement(By.xpath("//*[@id=\"content\"]/iframe"));

        driver.switchTo().frame(element);

        driver.findElement(By.xpath("//*[@id=\"selectable\"]/li[1]")).click();

        driver.switchTo().defaultContent();

        driver.findElement(By.xpath("//*[@id=\"content\"]/h1")).click();
    }
}



