package pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    public static WebDriver driver;

    @BeforeMethod()
    public void setupDriver() {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        //driver.manage().window().maximize();
        navigateToGoogle();

    }

    public static void waitForElement(WebElement element, int Time) {

        WebDriverWait wait = new WebDriverWait(driver, Time);
        wait.until(ExpectedConditions.visibilityOf(element));


    }
    public void navigateToGoogle() {
        driver.navigate().to("https://www.google.com/");
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterMethod(enabled = true)
    public void tearDown() {
        driver.quit();
    }
}
