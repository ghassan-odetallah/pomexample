package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class WindowManagement extends BaseTest {

    @Test
    public void windowManagement() {

        driver.get("https://www.layalina.com/");

        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"page-content\"]/div/div[1]/div/ul/li[1]/div/div[1]/a[1]/picture/imgw")).click();

    }

    public static void waitForElement(WebElement element, int Time) {

        WebDriverWait wait = new WebDriverWait(driver, Time);
        wait.until(ExpectedConditions.visibilityOf(element));

    }

    @Test
    public void testWait() {

        driver.get("https://www.layalina.com/");
        driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
        WebElement element = driver.findElement(By.xpath("//*[@id=\"slide-nav\"]/div[2]/div/ul[1]/li[8]/h2/aaa"));

        driver.manage().window().getSize().getWidth();

        System.out.println(driver.manage().window().getSize().getHeight() + driver.manage().window().getSize().getWidth());

        driver.manage().window().setSize(new Dimension(2000, 1800));
        driver.manage().window().maximize();
        driver.manage().window().fullscreen();

    }
}
