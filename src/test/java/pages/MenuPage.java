package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;

import static org.openqa.selenium.By.linkText;


public class MenuPage extends BaseTest {

    @FindBy(linkText = "Department Stores")
    private WebElement departmentStoreMenu;

    public MenuPage(WebDriver driver) {

        PageFactory.initElements(driver, this);

    }


    public void clieckDepartmentStoreMenu() {
        waitForElement(this.departmentStoreMenu, 2000);
        this.departmentStoreMenu.click();
    }

    public void clickByLinkText(String linkText) {
        WebElement menuElement = driver.findElement(linkText(linkText));
        menuElement.click();

    }
}
