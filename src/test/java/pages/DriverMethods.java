package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class DriverMethods extends BaseTest {

    @Test
    public int getDriverHandle() {

        String originalWindow = driver.getWindowHandle();
        //Check we don't have other windows open already
        //assert driver.getWindowHandles().size() == 1;
        System.out.println(driver.getWindowHandles().size() + "this is the lenght of driver ");
        return driver.getWindowHandles().size();
    }

    @Test
    public void testGetDriverHandle() throws InterruptedException {
        openNewTab();
        Thread.sleep(5000);
        System.out.println("number of opened windows are  " + getDriverHandle());

    }

    public void openNewTab() {

        ((JavascriptExecutor) driver).executeScript("window.open()");
    }

    @Test
    public void opensiteInNewTab() throws InterruptedException {

        openNewTab();
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));


        driver.navigate().to("https://www.layalina.com/");

        driver.switchTo().window(tabs.get(0));
        Thread.sleep(3000);


    }
}
