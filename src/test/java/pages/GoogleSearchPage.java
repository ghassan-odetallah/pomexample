package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class GoogleSearchPage extends BaseTest {

    @FindBy(xpath = "//body/div[1]/div[3]/form[1]/div[1]/div[1]/div[1]/div[1]/div[2]/input[1]")
    private WebElement searchText;
    @FindBy(xpath = "//div[contains(text(),'الأردن')]")
    private WebElement countryDev;
    @FindBy(xpath = "//body/div[1]/div[3]/form[1]/div[1]/div[1]/div[3]/center[1]/input[1]")
    private WebElement searchButton;
    @FindBy(xpath = "//span[contains(text(),'Ghassan Pharmacy')]")
    private static WebElement verificationLink;

    public WebElement getVerificationLink() {
        return verificationLink;
    }
    public static String  getVerificationLinkText() {
        return verificationLink.getText();
    }
    public GoogleSearchPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

    public void fillSearchText(String searchText) {
        this.searchText.sendKeys(searchText);
    }

    public void search(String searchText) {

        this.fillSearchText(searchText);
        clickSearch();
    }

    public void clickSearch() {
        countryDev.click();
        searchButton.click();
        System.out.println("this is the duplicated code");
    }

    public void search(String searchFirst, String searchSecond) {

        this.fillSearchText(searchFirst + searchSecond);
        clickSearch();

    }


    public void newSearch() {

        this.searchText.sendKeys("ghassam");
        this.countryDev.click();
        this.searchButton.click();

    }



}